with customers as (

    select * from {{ ref('stg_customers') }}

),

payment as (

    select * from {{ ref('stg_payment') }}

),

final as (

    select
        payment.customer_id,
        payment.payment_method,
        count(*) as number_of_orders,
        sum(payment.amount) as order_total_amount,
        customers.first_name,
        customers.last_name

    from payment
    left join customers on customers.customer_id = payment.customer_id
    where payment.status in ('success')
    group by payment.customer_id, payment.payment_method, customers.first_name, customers.last_name

)

select * from final