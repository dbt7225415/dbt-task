{{ config(materialized='view') }}

select
    customer_id,
    replace(paymentmethod, '_', ' ') as payment_method,
    amount,
    status

from {{ source('stripe', 'payment') }}
